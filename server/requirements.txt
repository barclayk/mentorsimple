

Flask==1.0.3
Flask-Cors==3.0.7
Flask-RESTful==0.3.7
SQLAlchemy==1.3.3
psycopg2==2.8.2
celery==4.3.0
gunicorn==19.9.0
gevent==1.4.0 
greenlet==0.4.15
redis 