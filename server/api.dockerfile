FROM python:3.6

RUN mkdir /api
WORKDIR /api

ADD api.py ./
ADD tasks.py ./
ADD requirements.txt ./

RUN pip install -r requirements.txt

EXPOSE 8000
CMD gunicorn -b 0.0.0.0:8000 api:app --worker-class gevent --workers 4 --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info
 