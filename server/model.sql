CREATE DATABASE dev;
GRANT ALL PRIVILEGES ON DATABASE dev TO postgres;
\connect dev;

CREATE TABLE users
    (
        id SERIAL PRIMARY KEY,
        uname TEXT NOT NULL,
        umobile TEXT,
        upwd TEXT
    );