from sqlalchemy import create_engine
from celery import Celery


# local dev
#celeryBroker = "redis://localhost:6379/0"
#engine = create_engine('postgres://postgres:postgres@localhost:5432/dev')

# docker
celeryBroker = "redis://redis:6379/0"
engine = create_engine('postgres://postgres:postgres@postgres:5432/dev')

worker = Celery("workers", broker=celeryBroker)


def exec_sql(sql_code):
    '''
        executes sql code - if no data returns complete
    '''
    sql = engine.execute(sql_code)
    try:
        data = [dict(i) for i in sql]
        return data
    except:
        return []
#exec_sql('select * from users;')


def getUsers():
    sql = 'select * from users;'
    sql = exec_sql(sql)
    return sql
#getUsers()


def checkEmail(username):
    sql = "select * from users where uname ='"+username+"';"
    sql = exec_sql(sql)
    return sql
#checkEmail('bk@gmail.com')

def getUser(username, pwd):
    sql = "select * from users where uname ='"+username+"' and upwd='"+pwd+"';"
    sql = exec_sql(sql)
    return sql
#getUser('bk@gmail.com', 'artis')


@worker.task
def insertUser(email, mobile, pwd):
    sql = "INSERT INTO users (uname, umobile, upwd) values ('" + \
        email+"',"+str(mobile)+",'"+pwd+"');"
    sql = exec_sql(sql)
    return sql

#insertUser('b@gmail.com', '1231241141', 'we')
