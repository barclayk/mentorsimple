FROM python:3.6

RUN mkdir /worker
WORKDIR /worker

ADD tasks.py ./
ADD requirements.txt ./

RUN pip install -r requirements.txt

CMD celery -A tasks worker --loglevel=info
 