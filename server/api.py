
from flask_cors import CORS
from flask_restful import Resource, Api, reqparse
from flask import Flask, jsonify
import tasks

app = Flask(__name__)
CORS(app)
api = Api(app, prefix="/v1")

prs = reqparse.RequestParser()
prs.add_argument('email', type=str)
prs.add_argument('mobile', type=int)
prs.add_argument('pwd', type=str)
prs.add_argument('confirmpwd', type=str)


class User(Resource):

    def get(self):
        try:
            usrs = tasks.getUsers()
            return usrs, 200
        except:
            return {'status': 'error'}, 405

    def post(self):
        args = prs.parse_args()
        email = args['email']
        mobile = int(args['mobile'])
        pwd = args['pwd']
        conf = args['confirmpwd']
        if pwd != conf:
            return {'status': 'Check pwd & confirm'}, 403

        check = tasks.checkEmail(email)
        if check == []:
            try:
                tasks.insertUser.delay(email, mobile, pwd)
                return {'status': 'initiated celery insert'}, 200
            except:
                return {'status': 'failure'}, 405
        else:
            return {'status': 'User exists'}, 403


class Login(Resource):
    def post(self):
        args = prs.parse_args()
        email = args['email']
        pwd = args['pwd']
        usr = []
        try:
            usr = tasks.getUser(email, pwd)
        except:
            return {'status': 'Check pwd & confirm'}, 403

        if usr == []:
            return {'status': 'Check pwd & confirm'}, 403
        else:
            print(usr, type(usr))
            usr[0]['user'] = email
            del usr[0]['uname']
            usr[0]['loggedin'] = 'true'
            return usr[0], 200


api.add_resource(User, '/user')
api.add_resource(Login, '/login')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=True)

'''
Assumed - postgres and redis running local on default ports

to run celery:
  celery -A worker worker --loglevel=info

to run api: 
  gunicorn -b 0.0.0.0:80 api:app --worker-class gevent --workers 8 --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info
'''
